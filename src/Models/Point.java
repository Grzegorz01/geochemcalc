package Models;
import java.util.*;

public abstract class Point
{
    public Coordinates PointCoordinates;

    public Point(double x, double y, double z)
    {
        this.PointCoordinates = new Coordinates(x, y, z);
    }

    public Point(double x, double y)
    {
        this.PointCoordinates = new Coordinates(x, y);
    }

    public static List<String> ChemicalNames = Arrays.asList(
            "Methane",
            "Ethane",
            "Propane",
            "IsoButhane",
            "NButhane",
            "Ethylene",
            "Propylene",
            "OneButhene",
            "C2C4SumAlkanes",
            "C2C4SumOlephines");

    public HashMap<String,Double> Chemicals = new HashMap<String,Double>();

}
