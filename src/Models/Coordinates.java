package Models;

/**
 * Created by Grzechu on 2016-12-16.
 */
public class Coordinates
{
    public Coordinates(double x, double y, double z)
    {
        this.X = x;
        this.Y = y;
        this.Z = z;
    }

    public Coordinates(double x, double y)
    {
        this.X = x;
        this.Y = y;
        this.Z = 0;
    }

    public double X;
    public double Y;
    public double Z;

}
